import React from 'react';
import { useParams } from 'react-router-dom';

export const PostPage = () => {
  const { slug } = useParams<{ slug: string }>();
  return (
    <div>
      {slug}
    </div>
  )
};
